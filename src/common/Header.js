import React from 'react';
import { useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { clearForecast } from '../features/Forecast/forecastSlice';
import { clearCity } from '../features/Search/searchSlice';
import { Button, Layout } from 'antd';

const { Header } = Layout;

const MyHeader = () => {
    const dispatch = useDispatch();
    const history = useHistory();

    function handleClick() {
        localStorage.removeItem('city');
        dispatch(clearCity());
        dispatch(clearForecast());
        history.push('/');
    }

    return (
        <Header
            style={{
                color: 'white',
                display: 'flex',
                justifyContent: 'space-between',
                alignItems: 'center',
            }}
        >
            <div>My Weather App</div>
            <Button type="primary" onClick={handleClick}>
                Change Location
            </Button>
        </Header>
    );
};

export default MyHeader;
