import React from 'react';
import { Layout } from 'antd';
import { Switch, Route, BrowserRouter as Router } from 'react-router-dom';
import './App.css';

import Header from '../common/Header';
import Search from '../features/Search/Search';
import ForecastList from '../features/Forecast/ForecastList/ForecastList';
import ForecastDetail from '../features/Forecast/ForecastDetail/ForecastDetail';

const { Footer, Content } = Layout;

function App() {
    return (
        <Layout>
            <Router>
                <Header />
                <Content
                    style={{
                        height: 'calc(100vh - 134px)',
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                    }}
                >
                    <Switch>
                        <Route exact path="/">
                            <Search />
                        </Route>
                        <Route exact path="/forecast">
                            <ForecastList />
                        </Route>
                        <Route exact path="/detailed/:date">
                            <ForecastDetail />
                        </Route>
                    </Switch>
                </Content>
                <Footer>Footer</Footer>
            </Router>
        </Layout>
    );
}

export default App;
