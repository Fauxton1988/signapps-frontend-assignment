import { combineReducers } from '@reduxjs/toolkit';
import searchReducer from '../features/Search/searchSlice';
import forecastReducer from '../features/Forecast/forecastSlice';

const rootReducer = combineReducers({
    search: searchReducer,
    forecast: forecastReducer,
});

export default rootReducer;
