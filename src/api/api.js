import axios from 'axios';

const instance = axios.create({
    baseURL: 'http://api.openweathermap.org/data/2.5/',
    headers: {
        'content-type': 'application/json',
    },
});

instance.interceptors.request.use(
    (response) => {
        return response;
    },
    (error) => {
        return Promise.reject(error);
    }
);

export const api = (function () {
    const apiKey = 'd8581e97e02d0b4ec881a66bd9c148b0';

    return {
        getForecastByCity: (city, units = 'standard') =>
            instance({
                method: 'GET',
                url: `forecast?q=${city}&appid=${apiKey}&units=${units}`,
            }),
    };
})();
