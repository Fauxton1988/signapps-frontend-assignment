import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { getWeatherForecast } from '../forecastSlice';
import { List } from 'antd';

import { toDailyForecasts, capitaliseCity } from '../../../utils/utils';

import ForecastItem from '../ForecastItem/ForecastItem';

import './ForecastList.css';

const ForecastList = () => {
    const dispatch = useDispatch();
    const city = useSelector(({ search }) => search.city);
    const forecasts = useSelector(({ forecast }) => forecast.forecasts);

    useEffect(() => {
        if (city) {
            dispatch(getWeatherForecast(city));
        }
    }, []);

    return (
        <div className="forecast-list-container">
            <h1>
                {forecasts.length
                    ? `5 day Forecast for ${capitaliseCity(city)}`
                    : ''}
            </h1>
            <List
                style={{ width: '80%' }}
                itemLayout="horizontal"
                dataSource={toDailyForecasts(forecasts)}
                renderItem={(item) => <ForecastItem item={item} />}
            />
        </div>
    );
};

export default ForecastList;
