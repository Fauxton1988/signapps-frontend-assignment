import React from 'react';
import { useHistory } from 'react-router-dom';
import { RightOutlined } from '@ant-design/icons';
import { formatDateTime, getForecastDate } from '../../../utils/utils';
import './ForecastItem.css';

const ForecastItem = ({ item }) => {
    const history = useHistory();

    return (
        <div
            className="forecast-list-item"
            onClick={() => {
                const date = getForecastDate(item);
                history.push(`/detailed/${date}`);
            }}
        >
            <div className="datetime">{formatDateTime(item.dt_txt)}</div>
            <div className="weather">{item.weather[0].description}</div>
            <div className="tempHigh">{`Temp high: ${item.main.temp_max} C`}</div>
            <div className="tempLow">{`Temp low: ${item.main.temp_min} C`}</div>
            <div className="clouds">{`Clouds: ${item.clouds.all} %`}</div>
            <div className="wind">{`Wind: ${item.wind.speed} m/s`}</div>
            <div className="icon">
                <RightOutlined />
            </div>
        </div>
    );
};

export default ForecastItem;
