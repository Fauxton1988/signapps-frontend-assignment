import React, { useEffect, useState } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { Card, Row, Col } from 'antd';
import { LeftOutlined } from '@ant-design/icons';
import './ForecastDetail.css';

import {
    createGraph,
    formatDateTime,
    getDailyForecast,
} from '../../../utils/utils';

const ForecastDetail = () => {
    const { date } = useParams();
    const history = useHistory();
    const [dailyForecast, setDailyForecast] = useState([]);

    const city = useSelector(({ search }) => search.city);
    const forecasts = useSelector(({ forecast }) => forecast.forecasts);

    useEffect(() => {
        if (forecasts.length) {
            setDailyForecast(getDailyForecast(forecasts, date));
        }
    }, []);

    useEffect(() => {
        if (dailyForecast.length) {
            createGraph('#myTemperatureChart', dailyForecast);
        }
    }, [dailyForecast]);

    const renderCard = (label, value = 0, unit) => {
        return (
            <Col xs={12} xl={6}>
                <Card className="detailed-card">
                    <div className="detailed-card-label">{label}:</div>
                    <div className="detailed-card-value">{`${value} ${
                        unit || ''
                    }`}</div>
                </Card>
            </Col>
        );
    };

    return (
        <div className="forecast-detail">
            <div
                className="detailed-head"
                onClick={() => history.push('/forecast')}
            >
                <LeftOutlined />
                <span>
                    {city}, {formatDateTime(date)}
                </span>
            </div>
            {dailyForecast.length ? (
                <Row gutter={16}>
                    {renderCard(
                        'Conditions',
                        dailyForecast[0].weather[0].description.toUpperCase()
                    )}
                    {renderCard('Temperature', dailyForecast[0].main.temp, 'C')}
                    {renderCard(
                        'Humidity',
                        dailyForecast[0].main.humidity,
                        '%'
                    )}
                    {renderCard(
                        'Pressure',
                        dailyForecast[0].main.pressure,
                        'hPa'
                    )}
                </Row>
            ) : null}
            <div style={{ height: '300px' }}>
                <canvas id="myTemperatureChart" />
            </div>
        </div>
    );
};

export default ForecastDetail;
