import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { api } from '../../api/api';

const searchInitialState = {
    forecasts: [],
    dailyforecast: [],
    status: 'idle',
    error: null,
};

export const getWeatherForecast = createAsyncThunk(
    'forecast/performSearch',
    async (city) => {
        const response = await api.getForecastByCity(city, 'metric');

        return response.data.list;
    }
);

const searchSlice = createSlice({
    name: 'forecast',
    initialState: searchInitialState,
    reducers: {
        statusToIdle: (state) => {
            state.status = 'idle';
        },
        clearErrorMessage: (state) => {
            state.error = null;
        },
        clearForecast: (state) => {
            state.forecast = [];
        },
    },
    extraReducers: {
        [getWeatherForecast.fulfilled]: (state, { payload }) => {
            state.status = 'succeeded';
            state.forecasts = payload;
        },
        [getWeatherForecast.pending]: (state) => {
            state.status = 'loading';
        },
        [getWeatherForecast.rejected]: (state, action) => {
            state.status = 'failed';
            state.error = action.error.message;
        },
    },
});

export const {
    statusToIdle,
    clearErrorMessage,
    clearForecast,
} = searchSlice.actions;

export default searchSlice.reducer;
