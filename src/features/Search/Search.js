import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { updateCity } from './searchSlice';
import { Input } from 'antd';

const { Search } = Input;

const MySearch = () => {
    const dispatch = useDispatch();
    const history = useHistory();
    const city = useSelector(({ search }) => search.city);

    useEffect(() => {
        const city = localStorage.getItem('city');
        if (city) {
            history.push('/forecast');
        }
    }, []);

    const handleOnCityChange = (e) => {
        dispatch(updateCity(e.target.value));
    };

    return (
        <div style={{ width: '50% ', padding: '20px' }}>
            <label>Where do you live?</label>
            <Search
                placeholder="Your location"
                onChange={handleOnCityChange}
                value={city}
                onSearch={() => {
                    localStorage.setItem('city', city);
                    history.push('/forecast');
                }}
            />
        </div>
    );
};

export default MySearch;
