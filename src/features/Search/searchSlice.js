import { createSlice } from '@reduxjs/toolkit';

const searchInitialState = {
    city: localStorage.getItem('city') || '',
};

const searchSlice = createSlice({
    name: 'search',
    initialState: searchInitialState,
    reducers: {
        updateCity: (state, { payload }) => {
            state.city = payload;
        },
        clearCity: (state) => {
            state.city = '';
        },
    },
});

export const { updateCity, clearCity } = searchSlice.actions;

export default searchSlice.reducer;
