import Chart from 'chart.js';

export const formatDateTime = (dateTimeString) => {
    return new Date(dateTimeString).toLocaleDateString('en-gb', {
        weekday: 'long',
        year: 'numeric',
        month: 'long',
        day: 'numeric',
    });
};

export const getForecastDate = (forecast) => {
    return forecast.dt_txt.split(' ')[0];
};

export const getForecastTime = (forecast) => {
    return forecast.dt_txt.split(' ')[1];
};

export const toDailyForecasts = (forecasts) => {
    const forecastTime = forecasts[0] && getForecastTime(forecasts[0]);
    return forecasts.filter((entry) => {
        return entry.dt_txt.includes(forecastTime);
    });
};

export const getDailyForecast = (forecasts, date) => {
    return forecasts.filter((forecast) => getForecastDate(forecast) === date);
};

export const capitaliseCity = (city) => {
    return `${city.charAt(0).toUpperCase()}${city.slice(1)}`;
};

export const createGraph = (selector, dataArray) => {
    const ctxTemp = document.querySelector(selector);

    new Chart(ctxTemp, {
        type: 'line',
        data: {
            labels: [
                ...dataArray.map((forecast) => {
                    return forecast.dt_txt.split(' ')[1];
                }),
            ],

            datasets: [
                {
                    label: 'Temperature',
                    data: [
                        ...dataArray.map((forecast) => {
                            return forecast.main.temp;
                        }),
                    ],
                    backgroundColor: 'rgba(232, 188, 12, 0.5)',
                },
            ],
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
        },
    });
};
